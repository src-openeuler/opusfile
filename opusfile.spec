Name:          opusfile
Version:       0.12
Release:       3
Summary:       A high-level API provides seeking, decode, and playback of Opus streams
License:       BSD
URL:           https://www.opus-codec.org/
Source0:       https://downloads.xiph.org/releases/opus/%{name}-%{version}.tar.gz
Patch0000:     0001-fix-MemorySanitizer-use-of-uninitialized-value.patch
Patch0001:     CVE-2022-47021.patch

BuildRequires: gcc
BuildRequires: pkgconfig(ogg) >= 1.3
BuildRequires: pkgconfig(openssl)
BuildRequires: pkgconfig(opus) >= 1.0.1

%description
The opusfile library provides seeking, decode, and playback of Opus streams in the Ogg
container (.opus files) including over http(s) on posix and windows systems.
opusfile depends on libopus and libogg.The included opusurl library for http(s) access
depends on opusfile and openssl.

%package devel
Summary:       Development package for opusfile package
Requires:      %{name} = %{version}-%{release} pkgconfig

%description devel
Development package for opusfile package.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
%make_build

%install
%make_install
%delete_la

%files
%license COPYING
%doc AUTHORS
%{_libdir}/libopusfile.so.*
%{_libdir}/libopusurl.so.*

%files devel
%doc %{_docdir}/%{name}
%{_includedir}/opus/opus*
%{_libdir}/pkgconfig/opusfile.pc
%{_libdir}/pkgconfig/opusurl.pc
%{_libdir}/libopusfile.so
%{_libdir}/libopusurl.so

%changelog
* Tue Sep 03 2024 Funda Wang <fundawang@yeah.net> - 0.12-3
- cleanup spec

* Fri Aug 16 2024 wangkai <13474090681@163.com> - 0.12-2
- Remove rpath

* Wed Oct 18 2023 wangkai <13474090681@163.com> - 0.12-1
- Update to 0.12

* Sun Jan 29 2023 yaoxin <yaoxin30@h-partners.com> - 0.11-5
- Fix CVE-2022-47021

* Wed Jun 02 2021 zhaoyao<zhaoyao32@huawei.com> - 0.11-4
- fixs faileds: /bin/sh: gcc: command not found.

* Thu Dec 03 2020 maminjie <maminjie1@huawei.com> - 0.11-3
- fix MemorySanitizer: use-of-uninitialized-value

* Sat Nov 30 2019 daiqianwen <daiqianwen@huawei.com> - 0.11-2
- Package init

